# Classroom Resource
Classroom Resource is an app where teachers can post requests for classroom donations they need. Similar apps like donorschoose.org and adoptaclassroom.org already exist, but regardless, I wanted to make a mini-version for fun that can maybe be utilized on a smaller scale, for example, within a local school or district.

### About the Internship
Tangible AI is a part-time unpaid educational internship, 10-15 hrs/week, remote. I worked on a solo personal full-stack Django application. While working on this application, I met with my mentor weekly, CTO Hobson Lane, to plan, establish goals, and review code. I also attended some daily standup meetings to discuss daily work. For the final step, I presented my personal project virtually to the San Diego Python Users Group.

## Getting Started
Clone repository

      git clone https://gitlab.com/cassjes.m/classroom-resource.git
    
Navigate to the project folder

      cd classroom-resource
      
Create Virtual Environnment

      # conda
      conda create -n myenv python=3.8
      
      # Windows
      python -m venv myenv
      
      # Mac / Unix / WSL
      virtualenv env

Activate Virtual Environnment

      #conda
      conda activate myenv
      
      # Windows
      myenv/scripts/activate
      
      # Mac / Unix / WSL
      source env/bin/activate
      
Install requirements.txt

      pip install -r requirements.txt

Apply Migrations to Database

      python manage.py migrate
      
Run Application

      ./runserver.sh



## Personal Intern Project To-Do List
### Week 1
- [x] Brainstorm project ideas
- [x] Pick Project

### Week 2
- [x] Set up Django project
- [x] Create models and create test data w/ Python shell
- [x] Create Django superuser

### Week 3
- [x] Create GitLab Repo and push code
- [x] Move project plan to GitLab
- [x] Continue working w/ models (many-to-many relationships)
- [x] Configure Django Admin interface to show all model classes

### Week 4
- [x] Create View for Profile, Item, School
- [x] Create template connections with data {}

### Week 5
- [x] Create developer view to view all data (connections{} already created in prior week)
- [x] Create index/home view and list links to other views (include introduction and info about the app)
- [x] Stretch Goal: Create Profile view (create a view and form for creating a user)

### Week 6
- [x] Go through Real Python Tutorial for User Management
    - [x] Create register
    - [ ] Create login

### Week 7
- [x] Implement django logging
- [x] Implement class-based views (ex: template/model/list views)
- [x] Change Profile model to User

### Week 8 - 10
- [x] Add bootstrap front-end styling
- [x] Utilize more logging
- [x] Implement form validation
- [ ] Stretch Goal: Re-implement class-based views
CRUD Functions:
    - [ ] Register (partially works for user only, not profile model)
    - [x] Login
    - [ ] Reset Password
    - [x] Update/Delete Profile
- [x] Presentation Prep 
- [x] Presentation Day
