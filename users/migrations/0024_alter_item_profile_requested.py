# Generated by Django 4.0 on 2021-12-16 07:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0023_remove_profile_email'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='profile_requested',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='users.profile'),
        ),
    ]
