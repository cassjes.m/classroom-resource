from django import forms
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
# Default Django user model
from django.contrib.auth.models import User

from .models import Item, Profile

class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
        
class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email']

class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['name', 'grades', 'schools']

class ItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = '__all__'