from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

from .models import *
from .forms import ItemForm, UserUpdateForm, UserRegisterForm, ProfileUpdateForm
# from .decorators import unauthenticated_user, allowed_users, admin_only

import logging
logger = logging.getLogger('django')

# Create your views here.

def home(request):
    template = loader.get_template('users/index.html')
    
    schools = School.objects.all()
    profiles = Profile.objects.order_by('id').all()
    items = Item.objects.order_by('-date_created').all()
    
    total_profiles = profiles.count()
    total_needed = items.filter(status='Still Needed').count()
    total_fulfilled = items.filter(status='Fulfilled').count()
    
    context = {
        'schools': schools,
        'profiles': profiles,
        'items': items,
        'total_profiles': total_profiles,
        'total_needed': total_needed,
        'total_fulfilled': total_fulfilled
    }
    
    logger.info('Home template was rendered.')
    return HttpResponse(template.render(context, request))

def registerPage(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        form = UserRegisterForm()
    
        if request.method == 'POST':
            # Django default UserCreationForm handles hashing and making sure user doesn't already exist
            form = UserRegisterForm(request.POST)
            if form.is_valid():
                form.save()
                username = form.cleaned_data.get('username')
                messages.success(request, username + f' has been created! You are now able to log in.')
                logger.info('New user data saved.')
                logger.info('Redirecting to login page.')
                return redirect('login')
        else: 
            form = UserRegisterForm()
        
        context = {'form':form}
        
        logger.info('Register template was rendered.')
        return render(request, 'users/register.html', context)

def loginPage(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')
            
            user = authenticate(request, username=username, password=password)
            
            if user is not None:
                login(request, user)
                logger.info('Login successful. Redirecting to dashboard.')
                return redirect('dashboard')
            else: 
                messages.info(request, 'username or password is incorrect')
        
        logger.info('Login template was rendered.')
        return render(request, 'users/login.html')

def logoutUser(request):
    logout(request)
    logger.info('User has been logged out.')
    return redirect('login')

@login_required(login_url='login')
def dashboard(request):
    u_form = UserUpdateForm(request.POST, instance=request.user)
    p_form = ProfileUpdateForm(request.POST,request.FILES,instance=request.user.profile)
    items = Item.objects.filter(profile_requested_id=request.user.profile).all()

    context = {
        'u_form': u_form,
        'p_form': p_form, 
        'items':items
    }
    return render(request, 'users/dashboard.html', context)

def teacher(request, pk):
	profile = Profile.objects.get(id=pk)
	items = profile.item_set.order_by('-date_created').all()

	context = {
        'profile':profile, 
        'items':items,
    }
    
	return render(request, 'users/teacher.html', context)

# @login_required(login_url='login')
def updateProfile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST,request.FILES,instance=request.user.profile)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f'Your account has been updated!')
            logger.info('Redirecting to dashboard.')
            return redirect('dashboard')

    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'u_form': u_form,
        'p_form': p_form
    }

    return render(request, 'users/profile_form.html', context)

@login_required(login_url='login')
def deleteProfile(request, pk):
    user = User.objects.get(id=pk)
    if request.method == 'POST':
        user.delete()
        logger.info('Profile was deleted.')
        logger.info('Redirecting to dashboard.')
        return redirect('/')

    logger.info('Delete template was rendered.')
    return render(request, 'users/profile_delete.html')


def items(request):
    profiles = Profile.objects.all()
    items = Item.objects.order_by('-status').all()
    
    context = {'profiles':profiles, 'items':items}
    logger.info('Items template was rendered.')
    return render(request, 'users/items.html', context)

@login_required(login_url='login')
def createItem(request):
    form = ItemForm()
    if request.method == 'POST':
        form = ItemForm(request.POST)
        logger.info('Test Print: ', request.POST)
        if form.is_valid:
            form.save()
            logger.info('Item data was posted.')
            logger.info('Redirecting to profile.')
            return redirect('dashboard')
    
    context = {'form':form}
    
    logger.info('Item Form template was rendered.')
    return render(request, 'users/item_form.html', context)

@login_required(login_url='login')
def updateItem(request, pk):
    item = Item.objects.get(id=pk)
    form = ItemForm(instance=item)
    if request.method == 'POST':
        form = ItemForm(request.POST, instance=item)
        logger.info('Test Print: ', request.POST)
        if form.is_valid:
            form.save()
            logger.info('Item data was updated.')
            logger.info('Redirecting to profile.')
            return redirect('dashboard')
        
    context = {'form':form}
    
    logger.info('Update Item Form template was rendered.')
    return render(request, 'users/item_form.html', context)

# @login_required(login_url='login')
def deleteItem(request, pk):
    item = Item.objects.get(id=pk)
    if request.method == 'POST':
        item.delete()
        logger.info('Item data was deleted.')
        logger.info('Redirecting to profile.')
        return redirect('dashboard')
    
    context = {'item':item}
    logger.info('Delete template was rendered.')
    return render(request, 'users/item_delete.html', context)

def developer(request):
    template = loader.get_template('users/developer.html')
    
    schools = School.objects.order_by('name').all()
    profiles = Profile.objects.order_by('name').all()
    items = Item.objects.order_by('name').all()
    
    context = {
        'schools': schools,
        'profiles': profiles,
        'items': items
    }
    logger.info('Developer template was rendered.')
    return HttpResponse(template.render(context, request))