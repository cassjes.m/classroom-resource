from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Profile(models.Model):
    GRADES = (
        ('Pre-K','Pre-K'),
        ('Elementary','Elementary'),
        ('Junior High','Junior High'),
        ('High School','High School'),
        ('Multiple','Multiple'),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=100, null=True)
    date_joined = models.DateTimeField(auto_now_add=True, null=True, editable=False)
    grades = models.CharField(max_length=100, null=True, choices=GRADES)
    # Many-to-Many Relationship using ManyToManyField
    # A Teacher can have more than one School, and a School can have more than one Teacher
    schools = models.ManyToManyField("School")
    
    # Returns Teacher 'name' field as a string in admin panel
    def __str__(self):
        return f'{self.user.username} Profile'

    def save(self):
        super().save()
    
class Item(models.Model):
    CATEGORIES = (
        ('Books', 'Books'),
        ('Food/Drinks', 'Food/Drinks'),
        ('Software/Hardware', 'Software/Hardware'),
        ('Subscriptions', 'Subscriptions'),
        ('Supplies', 'Supplies'),
    )
    STATUS = (
        ('Still Needed', 'Still Needed'),
        ('Fulfilled', 'Fulfilled'),
    )
    # Many-to-One Relationship using Foreign Key
    # Many Items can be associated to one Teacher
    profile_requested = models.ForeignKey("Profile", null=True, on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    name = models.CharField(max_length=100, null=True)
    category = models.CharField(max_length=100, null=True, choices=CATEGORIES)
    description = models.CharField(max_length=200, null=True)
    quantity = models.IntegerField(null=True)
    total = models.DecimalField(max_digits=6, decimal_places=2, null=True)
    status = models.CharField(max_length=200, null=True, choices=STATUS)
    
    def __str__(self):
        return self.name

class School(models.Model):
    name = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.name