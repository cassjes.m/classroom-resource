from django.urls import path
from . import views

urlpatterns = [
    # Home
    path('', views.home, name='home'),
    # Registration/Login/Logout
    path('register/', views.registerPage, name='register'),
    path('login/', views.loginPage, name='login'),
    path('logout/', views.logoutUser, name='logout'),
    # Profile - Personal
    path('dashboard/', views.dashboard, name='dashboard'),
    path('update_profile/', views.updateProfile, name='update_profile'),
    path('delete_profile/<str:pk>/', views.deleteProfile, name='delete_profile'),
    # Profile - Teacher Public Info
    path('teacher/<str:pk>/', views.teacher, name='teacher'),
    path('items/', views.items, name='items'),
    # Item
    path('create_item/', views.createItem, name='create_item'),
    path('update_item/<str:pk>/', views.updateItem, name='update_item'),
    path('delete_item/<str:pk>/', views.deleteItem, name='delete_item'),
    # Developer View 
    path('developer/', views.developer, name='developer'),
]