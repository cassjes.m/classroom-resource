from django.contrib import admin
from .models import Profile, Item, School

# Register your models here.

admin.site.register(Profile)
admin.site.register(Item)
admin.site.register(School)